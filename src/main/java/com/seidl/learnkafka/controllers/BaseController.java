package com.seidl.learnkafka.controllers;

import com.seidl.learnkafka.ProducerKafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author
 *      <a href="mailto:seidl.matus@gmail.com">
 *              Matus Seidl
 *      </a>
 *
 * 2017-12-05
 */
@RestController
public class BaseController {

    @Autowired
    private ProducerKafka producerKafka;

    @RequestMapping(value = "/home",method = RequestMethod.GET)
    public String index(@RequestParam("input")String input){
        producerKafka.sendMessage(input);
        return "Index: Kafka tutorials";
    }




}
