package com.seidl.learnkafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnkafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnkafkaApplication.class, args);
	}
}
