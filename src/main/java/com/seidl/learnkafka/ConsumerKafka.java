package com.seidl.learnkafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author
 *      <a href="mailto:seidl.matus@gmail.com">
 *              Matus Seidl
 *      </a>
 *
 * 2017-12-05
 */
@Component
public class ConsumerKafka {

    @KafkaListener( topics = {"${kafka.topic}"})
    public void onMessage(ConsumerRecord record){
        System.out.println("Read record is: "+ record.value());
    }
}
